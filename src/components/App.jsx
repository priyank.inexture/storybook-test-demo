import React from 'react';
import { hot } from 'react-hot-loader/root';
// import styles from '../styles/components/App.scss';
// import Logo from '../assets/images/React-icon.svg';
import Todo from './Todo';
function App() {
    return (
        <div
            style={{
                padding: '5vw 10vw'
            }}
        >
            <h1>Todo Application</h1>
            <Todo />
        </div>
    );
}
export default hot(App);
