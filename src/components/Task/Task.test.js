/* eslint-disable no-undef */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { Completed, Incomplete } from './Task.stories';
import '@testing-library/jest-dom/extend-expect';
test('renders the Completed task correctly', () => {
    render(<Completed {...Completed.args} />);
    // screen.debug(screen.getByTestId('form'));
    // expect(screen.getByRole('button')).toHaveClass('primary');
});
test('renders the Incompleted task correctly', () => {
    render(<Incomplete {...Incomplete.args} />);
    // screen.debug(screen.getByTestId('form'));
    // expect(screen.getByRole('button')).toHaveClass('primary');
});
