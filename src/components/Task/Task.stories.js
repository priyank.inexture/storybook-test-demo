import React from 'react';
import Task from './index';
export default {
    title: 'Primitive/Task',
    component: Task,
    argTypes: {
        onClick: {
            action: 'clicked'
        }
    }
};
const Template = (args) => <Task {...args} />;
Template.parameters = {
    jest: ['Task.test.js']
};
export const Incomplete = Template.bind({});
Incomplete.args = {
    completed: false,
    onClick: (id) => {
        console.log('will change complete to true', id);
    }
};
export const Completed = Template.bind({});
Completed.args = {
    completed: true,
    onClick: (id) => {
        console.log('will change complete to false', id);
    }
};
