import React from 'react';
import PropTypes from 'prop-types';
import './Task.css';
function Task({ id, task, dueDate, completed, onClick, ...rest }) {
    return (
        <div data-testid={`task-container-${id}`} className="task-container" onClick={() => onClick(id)} {...rest}>
            <div data-testid="task-title">{task}</div>
            <div
                data-testid="task-dueDate"
                style={{
                    textAlign: 'right'
                }}
            >
                {dueDate.toLocaleDateString()}
            </div>
            {completed && <div className="strike-text" data-testid={`task-strike-${id}`} />}
        </div>
    );
}
Task.propTypes = {
    id: PropTypes.number,
    task: PropTypes.string,
    dueDate: PropTypes.instanceOf(Date),
    completed: PropTypes.bool,
    onClick: PropTypes.func
};
Task.defaultProps = {
    id: 0,
    task: 'Task description goes here!',
    dueDate: new Date(),
    completed: false,
    onClick: (id) => {
        console.log('will change complete', id);
    }
};
export default Task;
