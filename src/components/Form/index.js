/* eslint-disable react/prop-types */
import React from 'react';
import Button from '../Button';
import { DateInput, TextInput } from '../Input';
import PropTypes from 'prop-types';
import './Form.css';
import TaskSchema from '../../validations/TaskSchema';
import { Formik } from 'formik';
const today = new Date();
today.setHours(0, 0, 1, 0);
function Form({ onSubmitCallback, task, dueDate }) {
    return (
        <Formik
            enableReinitialize
            validationSchema={TaskSchema}
            onSubmit={(values, { setSubmitting, resetForm }) => {
                // console.log('here submiteed');
                onSubmitCallback(values);
                setSubmitting(false);
                resetForm();
            }}
            initialValues={{ task: task, dueDate: dueDate }}
        >
            {(props) => {
                const { submitForm, values, touched, errors, isSubmitting, handleChange, handleBlur } = props;
                // console.log('errors', errors);
                return (
                    <form
                        data-testid="form"
                        className="form-container"
                        onSubmit={(e) => {
                            e.preventDefault();
                            console.log('submiting form');
                            submitForm();
                        }}
                    >
                        <div style={{ display: 'flex', flexDirection: 'column' }}>
                            <TextInput name="task" label="Task" error={errors.task && touched.task && true} value={values.task} onChange={handleChange} onBlur={handleBlur} />
                            {errors.task && touched.task && <span className="input-feedback">{errors.task}</span>}
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column' }}>
                            <DateInput name="dueDate" label="Due by" error={errors.dueDate && touched.dueDate && true} value={new Date(values.dueDate)} onChange={handleChange} onBlur={handleBlur} />
                            {errors.dueDate && touched.dueDate && <span className="input-feedback">{errors.dueDate}</span>}
                        </div>
                        <Button
                            label="Add"
                            variant="primary"
                            size="Basic"
                            type="submit"
                            disabled={isSubmitting}
                            // onClick={() => {
                            //     console.log('going to trigger');
                            //     submitForm();
                            // }}
                        />
                        {/* <button>Submit</button> */}
                    </form>
                );
            }}
        </Formik>
    );
}
Form.propTypes = {
    /**
     * onSubmitCallback Handler
     */
    onSubmitCallback: PropTypes.func,
    /**
     * Task description
     */
    task: PropTypes.string,
    /**
     * Due date in milliseconds
     */
    dueDate: PropTypes.instanceOf(Date)
};
Form.defaultProps = {
    onSubmitCallback: () => {
        console.log('submitted callback');
    },
    task: '',
    dueDate: today
};
export default Form;
