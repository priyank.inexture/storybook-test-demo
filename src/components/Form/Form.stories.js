import React from 'react';
import Form from './index';
export default {
    title: 'Form',
    component: Form,
    argTypes: { onSubmit: { action: 'clicked' } }
};

const Template = (args) => <Form {...args} />;
Template.parameters = {
    jest: ['Form.test.js']
};
export const Basic = Template.bind({});
// export const Errors = Template.bind({});
Basic.args = {
    onSubmit: () => {
        console.log('submitted');
    },
    task: ''
};
// Errors.args = {
//     onSubmit: () => {
//         console.log('submitted');
//     },
//     task: ''
// };
