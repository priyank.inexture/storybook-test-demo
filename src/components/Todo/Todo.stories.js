import React from 'react';
import Todo from './index';
export default {
    title: 'Todo',
    component: Todo
    // argTypes: {
    //     onClick: {
    //         action: 'clicked'
    //     }
    // }
};
const Template = (args) => <Todo {...args} />;
Template.parameters = {
    jest: ['Todo.test.js']
};
export const Basic = Template.bind({});
Basic.args = {
    initialTasks: [
        { id: 1, task: 'this is first Task', dueDate: new Date(2021, 5, 7), completed: false },
        { id: 2, task: 'this is second Task', dueDate: new Date(2021, 5, 7), completed: true },
        { id: 3, task: 'this is third Task', dueDate: new Date(2021, 5, 7), completed: false }
    ]
};
export const Empty = Template.bind({});
Empty.args = {
    initialTasks: []
};
