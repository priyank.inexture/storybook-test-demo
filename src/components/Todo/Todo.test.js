/* eslint-disable no-undef */
import React from 'react';
import { waitFor, fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Basic } from './Todo.stories';
import '@testing-library/jest-dom/extend-expect';
test('renders the todo list correctly', () => {
    render(<Basic {...Basic.args} />);
    screen.debug(screen.getByTestId('form'));
});
test('Rendering of Labels and inputs', async() => {
    const { getByTestId } = render(<Basic {...Basic.args} />);
    const todoElement = screen.getByTestId('Todo');
    const titleInput = await waitFor(() => getByTestId('text-input'));
    const dateInput = await waitFor(() => getByTestId('date-input'));
    const submitButton = screen.getByRole('button', { name: 'Add' });
    expect(todoElement).toBeInTheDocument();
    expect(titleInput).toBeInTheDocument();
    expect(dateInput).toBeInTheDocument();
    expect(submitButton).toBeInTheDocument();
});
test('Adding a new Task without any errors', async() => {
    const { getByTestId } = render(<Basic {...Basic.args} />);
    const todoElement = screen.getByTestId('Todo');
    const titleInput = await waitFor(() => getByTestId('text-input'));
    const dateInput = await waitFor(() => getByTestId('date-input'));
    const submitButton = screen.getByRole('button', { name: 'Add' });
    await userEvent.type(titleInput, 'testtask', { delay: 1 });
    await waitFor(() => fireEvent.change(dateInput, { target: { value: '2025-11-06' } }));
    await waitFor(() => fireEvent.click(submitButton));
    expect(todoElement).toHaveTextContent('testtask');
    expect(todoElement).toHaveTextContent('6/11/2025');
});
test('Changing status of a task', async() => {
    const { getByTestId } = render(<Basic {...Basic.args} />);
    const todoElement = screen.getByTestId('Todo');
    const titleInput = await waitFor(() => getByTestId('text-input'));
    const dateInput = await waitFor(() => getByTestId('date-input'));
    const submitButton = screen.getByRole('button', { name: 'Add' });
    await userEvent.type(titleInput, 'testtask', { delay: 1 });
    await waitFor(() => fireEvent.change(dateInput, { target: { value: '2025-11-06' } }));
    await waitFor(() => fireEvent.click(submitButton));
    expect(todoElement).toHaveTextContent('testtask');
    expect(todoElement).toHaveTextContent('6/11/2025');
    const taskElement = screen.getByTestId('task-container-4');
    expect(() => getByTestId('task-strike-4')).toThrow();
    fireEvent.click(taskElement);
    expect(screen.getByTestId('task-strike-4')).toBeInTheDocument();
});
test('Past date as due date', async() => {
    const { getByTestId } = render(<Basic {...Basic.args} />);
    const todoElement = screen.getByTestId('Todo');
    const titleInput = await waitFor(() => getByTestId('text-input'));
    const dateInput = await waitFor(() => getByTestId('date-input'));
    const submitButton = screen.getByRole('button', { name: 'Add' });
    await userEvent.type(titleInput, 'testtask', { delay: 1 });
    await waitFor(() => fireEvent.change(dateInput, { target: { value: '2000-06-19' } }));
    await waitFor(() => fireEvent.click(submitButton));
    expect(todoElement).not.toHaveTextContent('testtask');
    expect(todoElement).not.toHaveTextContent('19/6/2000');
    expect(todoElement).toHaveTextContent(/Date Cannot be in the past/);
});
test('Too short Task description', async() => {
    const { getByTestId } = render(<Basic {...Basic.args} />);
    const todoElement = screen.getByTestId('Todo');
    const titleInput = await waitFor(() => getByTestId('text-input'));
    const dateInput = await waitFor(() => getByTestId('date-input'));
    const submitButton = screen.getByRole('button', { name: 'Add' });
    await userEvent.type(titleInput, 'X', { delay: 1 });
    await waitFor(() => fireEvent.change(dateInput, { target: { value: '2022-06-19' } }));
    await waitFor(() => fireEvent.click(submitButton));
    expect(todoElement).not.toHaveTextContent('X');
    // expect(todoElement).not.toHaveTextContent('19/6/2022');
    expect(todoElement).toHaveTextContent(/Title must be more than 2 characters/);
});
test('Long Task description', async() => {
    const { getByTestId } = render(<Basic {...Basic.args} />);
    const todoElement = screen.getByTestId('Todo');
    const titleInput = await waitFor(() => getByTestId('text-input'));
    const dateInput = await waitFor(() => getByTestId('date-input'));
    const submitButton = screen.getByRole('button', { name: 'Add' });
    let str = '';
    for (let i = 0; i < 52; i++) {
        str += 'a';
    }
    await userEvent.type(titleInput, str, { delay: 1 });
    await waitFor(() => fireEvent.change(dateInput, { target: { value: '2022-06-19' } }));
    await waitFor(() => fireEvent.click(submitButton));
    expect(todoElement).toHaveTextContent(/Title must be less than 50 characters/);
});
