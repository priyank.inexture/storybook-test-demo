import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './Todo.css';
import Form from '../Form';
import Task from '../Task';
const today = new Date();
today.setHours(0, 0, 0, 0);
function Todo({ initialTasks, handleCompleteTask, ...rest }) {
    const [tasks, setTasks] = useState(initialTasks);
    const handleFormSubmitCallback = (values) => {
        // console.log('new values', values);
        let id = -1;
        tasks.forEach((t) => (id < t.id ? (id = t.id) : null));
        id++;
        setTasks((prevTasks) => [...prevTasks, { id: id, task: values.task, dueDate: new Date(values.dueDate), completed: false }]);
    };
    return (
        <div data-testid="Todo">
            <Form onSubmitCallback={handleFormSubmitCallback} task="" dueDate={new Date()} />
            <div
                style={{
                    marginTop: 20
                }}
            >
                {tasks.length > 0 &&
                    tasks
                        .sort((a, b) => a.dueDate.getTime() - b.dueDate.getTime())
                        .filter((t) => t.dueDate.getTime() >= today.getTime())
                        .map((t) => <Task key={t.id} onClick={() => handleCompleteTask(t.id, tasks, setTasks)} {...t} />)}
                {tasks.length === 0 && <h3>No task to perform!</h3>}
            </div>
        </div>
    );
}
Todo.propTypes = {
    /**
     * Array of task
     */
    initialTasks: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number,
            task: PropTypes.string,
            dueDate: PropTypes.instanceOf(Date),
            completed: PropTypes.bool
        })
    ),
    /**
     * handler for task status
     */
    handleCompleteTask: PropTypes.func
};
Todo.defaultProps = {
    initialTasks: [],
    handleCompleteTask: (id, tasks, setTasks) => {
        const task = tasks.find((t) => t.id === id);
        if (task) {
            task.completed = !task.completed;
            setTasks((prevTasks) => prevTasks.map((t) => (t !== id ? t : task)));
        }
    }
};
export default Todo;
