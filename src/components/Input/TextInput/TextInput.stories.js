import React from 'react';
import TextInput from './index';
export default {
    title: 'Input/Text',
    component: TextInput
    // argTypes: {
    //     value: {
    //         controls: {
    //             disable: true
    //         }
    //     },
    // }
};
const Template = (args) => <TextInput {...args} readOnly />;
Template.parameters = {
    jest: ['TextInput.test.js']
};
export const Basic = Template.bind({});
export const Errors = Template.bind({});
Basic.args = {
    label: 'Label',
    name: 'input-text',
    placeholder: 'Enter here!',
    error: false,
    value: ''
};
Errors.args = {
    label: 'Label',
    name: 'input-text',
    placeholder: 'Enter here!',
    error: true,
    value: ''
};
