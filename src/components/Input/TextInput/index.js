import React from 'react';
import PropTypes from 'prop-types';
import './TextInput.css';
function TextInput({ label, name, placeholder, value, error, ...rest }) {
    return (
        <label className={`input-label${error ? '-error' : ''}`} data-testid="text-label">
            {label}
            <input name={name} type="text" value={value} placeholder={placeholder} className={`input-text${error ? '-error' : ''}`} {...rest} data-testid="text-input" />
        </label>
    );
}
TextInput.propTypes = {
    /**
     * Label of the input text
     */
    label: PropTypes.string,
    /**
     * Name of the input text
     */
    name: PropTypes.string,
    /**
     * placeholder of text input
     */
    placeholder: PropTypes.string,
    /**
     * value of text input
     */
    value: PropTypes.string,
    /**
     * has error
     */
    error: PropTypes.bool
};
TextInput.defaultProps = {
    label: 'Input Label',
    name: 'input-text',
    placeholder: 'Enter here!',
    error: false,
    value: ''
};
export default TextInput;
