/* eslint-disable no-undef */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { Basic, Errors } from './TextInput.stories';
import '@testing-library/jest-dom/extend-expect';
test('renders the basic text input correctly', () => {
    render(<Basic {...Basic.args} readOnly />);
    const inputElement = screen.getByTestId('text-input');
    const labelElement = screen.getByTestId('text-label');
    expect(inputElement).toHaveClass('input-text');
    expect(labelElement).toHaveClass('input-label');
});
test('renders the errored text input correctly', () => {
    render(<Errors {...Errors.args} readOnly />);
    const inputElement = screen.getByTestId('text-input');
    const labelElement = screen.getByTestId('text-label');
    expect(inputElement).toHaveClass('input-text-error');
    expect(labelElement).toHaveClass('input-label-error');
});
