import React from 'react';
import DateInput from './index';
export default {
    title: 'Input/Date',
    component: DateInput,
    argTypes: {
        value: { control: 'date' }
    }
};
const Template = (args) => <DateInput {...args} />;
Template.parameters = {
    jest: ['DateInput.test.js']
};
export const Basic = Template.bind({});
export const Errors = Template.bind({});
Basic.args = {
    label: 'Label',
    name: 'input-date',
    error: false
};
Errors.args = {
    label: 'Label',
    name: 'input-date',
    error: true
};
