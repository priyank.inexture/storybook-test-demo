import React from 'react';
import PropTypes from 'prop-types';
import './DateInput.css';
const today = new Date();
function DateInput({ label, name, value, error, ...rest }) {
    const date = new Date(value);
    const dateString = `${date.getUTCFullYear()}-${(date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1)}-${(date.getDate() < 9 ? '0' : '') + date.getDate()}`;
    return (
        <>
            <label className={`input-label${error ? '-error' : ''}`} data-testid="date-label">
                {label}
                <input name={name} type="date" value={dateString} className={`input-date${error ? '-error' : ''}`} {...rest} data-testid="date-input" />
            </label>
        </>
    );
}
DateInput.propTypes = {
    /**
     * Label of the date input
     */
    label: PropTypes.string,
    /**
     * Name of the date input
     */
    name: PropTypes.string,
    /**
     * value of date input
     */
    value: PropTypes.instanceOf(Date),
    /**
     * has error
     */
    error: PropTypes.bool
};
DateInput.defaultProps = {
    label: 'Label',
    name: 'input-date',
    error: false,
    value: today
};
export default DateInput;
