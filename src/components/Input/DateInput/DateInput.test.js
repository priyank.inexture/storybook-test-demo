/* eslint-disable no-undef */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { Basic, Errors } from './DateInput.stories';
import '@testing-library/jest-dom/extend-expect';
test('renders the basic date input correctly', () => {
    render(<Basic {...Basic.args} readOnly />);
    const inputElement = screen.getByTestId('date-input');
    const labelElement = screen.getByTestId('date-label');
    expect(inputElement).toHaveClass('input-date');
    expect(labelElement).toHaveClass('input-label');
});
test('renders the errored date input correctly', () => {
    render(<Errors {...Errors.args} readOnly />);
    const inputElement = screen.getByTestId('date-input');
    const labelElement = screen.getByTestId('date-label');
    expect(inputElement).toHaveClass('input-date-error');
    expect(labelElement).toHaveClass('input-label-error');
});
