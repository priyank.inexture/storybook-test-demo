import React from 'react';
import Button from './index';
export default {
    title: 'Input/Button',
    component: Button,
    argTypes: {
        onClick: {
            action: 'clicked'
        }
    }
};
const Template = (args) => <Button {...args} />;
Template.parameters = {
    jest: ['Button.test.js']
};
export const Basic = Template.bind({});
Basic.args = {
    label: 'button',
    size: 'Basic',
    variant: 'primary'
};
export const Large = Template.bind({});
Large.args = {
    label: 'button',
    size: 'Large',
    variant: 'primary'
};
