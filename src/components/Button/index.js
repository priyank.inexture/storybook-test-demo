import React from 'react';
import PropTypes from 'prop-types';
import './Button.css';
function Button({ onClick, size, label, variant, ...rest }) {
    return (
        <button className={`button ${variant} ${size}`} onClick={onClick} {...rest}>
            {label}
        </button>
    );
}
Button.propTypes = {
    /**
     * Button Content
     */
    label: PropTypes.string,
    /**
     * Is button primary or secondary
     */
    variant: PropTypes.oneOf(['primary', 'secondary']),
    /**
     * Size of button
     */
    size: PropTypes.oneOf(['Basic', 'Large']),
    /**
     * onClick Handler
     */
    onClick: PropTypes.func
};
Button.defaultProps = {
    label: 'button',
    variant: 'primary',
    size: 'Basic',
    onClick: () => {
        console.log('clicked here');
    }
};
export default Button;
