/* eslint-disable no-undef */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { Basic, Large } from './Button.stories';
import '@testing-library/jest-dom/extend-expect';
test('renders the button in the Basic state', () => {
    render(<Basic {...Basic.args} />);
    // screen.debug(screen.getByRole('button'));
    expect(screen.getByRole('button')).toHaveClass('Basic');
    expect(screen.getByRole('button')).toHaveClass('primary');
});
test('renders the button in the Large state', () => {
    render(<Large {...Large.args} />);
    expect(screen.getByRole('button')).toHaveClass('primary');
    expect(screen.getByRole('button')).toHaveClass('Large');
});
