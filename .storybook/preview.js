import { addDecorator } from '@storybook/react'; // <- or your view layer
import { withTests } from '@storybook/addon-jest';
import { withA11y } from '@storybook/addon-a11y';
import results from '../.jest-test-results.json';
addDecorator(withA11y);
addDecorator(
    withTests({
        results
    })
);
export const parameters = {
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
        matchers: {
            color: /(background|color)$/i,
            date: /Date$/
        }
    }
};
